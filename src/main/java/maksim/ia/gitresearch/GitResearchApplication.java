package maksim.ia.gitresearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitResearchApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(GitResearchApplication.class, args);
	}

}
